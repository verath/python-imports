Using `-m` switch from toplevel works without sys.path hack:

```
$ python -m bin.run_project_b
very cool abc
```

Executing script from toplevel requires sys.path hack (but only needed for the initial script in bin):

```
$ python ./bin/run_project_a_hack.py
abc
very cool abc
```

Inside bin both methods needs to use path hack:

```
$ cd bin && python ./run_project_a_hack.py
abc
very cool abc
```
