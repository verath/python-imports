import sys
import os
SCRIPT_DIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(SCRIPT_DIR, "..")))

from project_a.main import main

if __name__ == "__main__":
    main()
