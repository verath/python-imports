import sys
import os
SCRIPT_DIR = os.path.dirname(__file__)
sys.path.insert(0, os.path.abspath(os.path.join(SCRIPT_DIR, "..")))

import nested.project_b.funcs

if __name__ == "__main__":
    nested.project_b.funcs.cool_func()
